---
layout: post
title: "ARDC sponsors the Reproducible Builds project"
date: 2020-09-25 00:00:00
categories: org
draft: false
---

<big>
**The Reproducible Builds project is pleased to announce a donation from
[Amateur Radio Digital Communications](https://ampr.org/) (ARDC) in support
of its goals.** ARDC's contribution will propel the Reproducible Builds
project's efforts in ensuring the future health, security and sustainability of
our increasingly digital society.
</big>

## About Amateur Radio Digital Communications (ARDC)

[![]({{ "/images/news/ardc-sponsors-the-reproducible-builds-project/ardc.png#right" | relative_url }})]({{ "https://ampr.org/" | relative_url }})

[Amateur Radio Digital Communications](https://ampr.org/) (ARDC) is a
non-profit that was formed to further research and experimentation with digital
communications using radio, with a goal of advancing the state of the art of
amateur radio and to educate radio operators in these techniques.

It does this by managing the allocation of network resources, encouraging
research and experimentation with networking protocols and equipment,
publishing technical articles and number of other activities to promote the
public good of amateur radio and other related fields. ARDC has recently begun
to contribute funding to organisations, groups, individuals and projects
towards these and related goals, and their grant to the Reproducible Builds
project is part of this new initiative.

Amateur radio is an entirely volunteer activity performed by knowledgeable
hobbyists who have proven their ability by passing the appropriate government
examinations. No remuneration is permitted. "Ham radio," as it is also known,
has proven its value in advancements of the state of the communications arts,
as well as in public service during disasters and in times of emergency.

For more information about ARDC, please see their website at
[ampr.org](https://ampr.org/).


## About the Reproducible Builds project

[![]({{ "/images/news/ardc-sponsors-the-reproducible-builds-project/reproducible-builds.png#right" | relative_url }})]({{ "/" | relative_url }})

One of the original promises of open source software was that peer review would
result in greater end-user security and stability of our digital ecosystem.
However, although it is theoretically possible to inspect and build the
original source code in order to avoid maliciously-inserted flaws, almost all
software today is distributed in prepackaged form.

This disconnect allows third-parties to compromise systems by injecting code
into seemingly secure software during the build process, as well as by
manipulating copies distributed from 'app stores' and other package repositories.

In order to address this, 'Reproducible builds' are a set of software
development practices, ideas and tools that create an independently-verifiable
path from the original source code, all the way to what is actually running on
our machines. Reproducible builds can reveal the injection of backdoors
introduced by the hacking of developers' own computers, build servers and
package repositories, but can also expose where volunteers or companies have
been coerced into making changes via blackmail, government order, and so on.

A world without reproducible builds is a world where our digital infrastructure
cannot be trusted and where online communities are slower to grow, collaborate
less and are increasingly fragile. Without reproducible builds, we leave space
for greater encroachments on our liberties both by individuals as well as
powerful, unaccountable actors such as governments, large corporations and
autocratic regimes.

The Reproducible Builds project began as a project within the Debian community,
but is now working with many crucial and well-known free software projects such
as Coreboot, openSUSE, OpenWrt, Tails, GNU Guix, Arch Linux, Tor, and many
others. It is now an entirely Linux distribution independent effort and serves
as the central 'clearing house' for all issues related to securing build
systems and software supply chains of all kinds.

For more about the Reproducible Builds project, please see their website at
[reproducible-builds.org]({{ "/" | relative_url }}).

<br>

<small>If you are interested in ensuring the ongoing security of the software that
underpins our civilisation, and wish to sponsor the Reproducible Builds
project, please reach out to the project by emailing
[contact@reproducible-builds.org](mailto:contact@reproducible-builds.org).</small>
