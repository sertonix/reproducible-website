---
layout: post
title:  "Reproducible Builds at FOSDEM 2024"
date: 2024-02-08
categories: org
---

[![]({{ "/images/news/2024-02-08-reproducible-builds-at-fosdem-2024/fosdem.jpeg#right" | relative_url }})](https://fosdem.org/2024/schedule/event/fosdem-2024-3353-reproducible-builds-the-first-ten-years/)

Core Reproducible Builds developer Holger Levsen presented at the main track at [FOSDEM](https://fosdem.org/2024/) on Saturday 3rd February this year in Brussels, Belgium. Titled *Reproducible Builds: The First Ten Years*…

> In this talk Holger 'h01ger' Levsen will give an overview about Reproducible Builds: How it started with a small BoF at DebConf13 (and before), then grew from being a Debian effort to something many projects work on together, until in 2021 it was mentioned in an Executive Order of the President of the United States. And of course, the talk will not end there, but rather outline where we are today and where we still need to be going, until Debian stable (and other distros!) will be 100% reproducible, verified by many.
>
> h01ger has been involved in reproducible builds since 2014 and so far has set up automated reproducibility testing for Debian, Fedora, Arch Linux, FreeBSD, NetBSD and coreboot.

[![]({{ "/images/news/2024-02-08-reproducible-builds-at-fosdem-2024/video.png#center" | relative_url }})](https://fosdem.org/2024/schedule/event/fosdem-2024-3353-reproducible-builds-the-first-ten-years/)

More information can be found on FOSDEM's [own page for the talk](https://fosdem.org/2024/schedule/event/fosdem-2024-3353-reproducible-builds-the-first-ten-years/), including a [video recording](https://video.fosdem.org/2024/k1105/fosdem-2024-3353-reproducible-builds-the-first-ten-years.av1.webm) and [slides](https://reproducible-builds.org/_lfs/presentations/2024-02-03-R-B-the-first-10-years/#/).

---

Separate from Holger's talk, however, there were a number of other talks about reproducible builds at FOSDEM this year:

* [Reproducible builds for confidential computing: Why remote attestation is worthless without it](https://fosdem.org/2024/schedule/event/fosdem-2024-1769-reproducible-builds-for-confidential-computing-why-remote-attestation-is-worthless-without-it/) by [Malte Poll](https://chaos.social/@malte) and [Paul Meyer](https://infosec.exchange/@katexochen).
* [RISC-V Bootstrapping in Guix and Live-Bootstrap](https://fosdem.org/2024/schedule/event/fosdem-2024-1755-risc-v-bootstrapping-in-guix-and-live-bootstrap/) by [Ekaitz](https://ekaitz.elenq.tech/).
* [rix: an R package for reproducible dev environments with Nix](https://fosdem.org/2024/schedule/event/fosdem-2024-1767-rix-an-r-package-for-reproducible-dev-environments-with-nix/) by [Bruno Rodrigues](https://www.brodrigues.co/).
* [Making reproducible and publishable large-scale HPC experiments](https://fosdem.org/2024/schedule/event/fosdem-2024-2651-making-reproducible-and-publishable-large-scale-hpc-experiments/) by [Philippe Swartvagher](https://ph-sw.fr/).
* [Documenting and Fixing Non-Reproducible Builds due to Configuration Options](https://fosdem.org/2024/schedule/event/fosdem-2024-2848-documenting-and-fixing-non-reproducible-builds-due-to-configuration-options/) by [RANDRIANAINA Georges Aaron](https://perso.eleves.ens-rennes.fr/people/georges-aaron.randrianaina/).

… and there was even an [entire track on Software Bill of Materials](https://fosdem.org/2024/schedule/track/software-bill-of-materials/).
