---
layout: event_detail
title: Collaborative Working Sessions - Born Reproducible II
event: hamburg2023
order: 303
permalink: /events/hamburg2023/born-reproducible-2/
---

Notes

This session is a follow-up on session from day 2 "Born Reproducible"

* Step #0 - Have a clean and defined build environment
* Step #1 - Know/Define:
  * input
  * output
* Step #2 - Record build metadata (e.g., buildinfo)
* Step #3 - Try to rebuild in a similar environment
* Step #4 - Analyse any differences
* Step #5 - Try to rebuild in a different enviroment
* Step #6 - Make it possible for users to rebuild/validate
* Step #7 - Define policy if build is unreproducible
* Step #8 - Collect build attestations
* Step #9 - Diversify Builders (e.g., identities, data centers, possibly users)
* Step #10 - Implement / Deploy Policy
* Profit! - Announce reproducibility
Communicate on every step!



