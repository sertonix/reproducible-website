---
layout: event_detail
title: Mapping the Big Picture
event: hamburg2023
order: 23
permalink: /events/hamburg2023/big-picture/
---

Building on the mappings we did at the 2022 Reproducible Builds Summit, the group will use this time to take stock of where things stand for Reproducible Builds across a range of context, as of the Summit. We'll identify success stories, exemplars and case studies to be celebrated and amplified, while also mapping challenges, needs and unsolved problems.

Topics, issues and ideas that surface during this session will inform how we structure the rest of the agenda.

[Success stories]({{ "/events/hamburg2023/success-stories/" | relative_url }})
* Real world success stories: What we know works
* Real world success stories we need or are searching for

[Projects]({{ "/events/hamburg2023/projects/" | relative_url }})
* Projects practicing reproducibility
* What projects/platforms/libraries do we *want*/need to be reproducible?

[Mapping projects infra]({{ "/events/hamburg2023/infra/" | relative_url }})
* Missing RB infrastructure we need to create

[Mapping lists]({{ "/events/hamburg2023/lists/" | relative_url }})
* Problems we need to discuss/solve
* Other topics we need to discuss
* Other lists we need to make
* Other projects/people we should get to the next Summit
