---
title: commandments of reproducible builds
layout: docs
permalink: /docs/commandments/
---

Commandments by the church of reproducible builds:

   1. Thou shall not record the name of thy maker nor the place of thy making (username, hostname)
   1. Thou shall not record the date nor time of thy making, unless you respect the holy [SDE spec](https://reproducible-builds.org/docs/source-date-epoch/) ([date+time](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/timestamp))
   1. Thou shall not use memory without initialization or use memory addresses to decide outcomes ([ASLR](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/aslr))
   1. Thou shall do all your work in order - not use [filesystem-readdir-order](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/filesystem) nor random order of hash elements
   1. Thou shall not *(gamble and)* record random numbers ([UUID, private/public key](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/random), [hash-seed](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/hash), ASLR)
   1. Thou shall only do one thing at a time or ensure races do no harm ([parallelism](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/race))
   1. Thou shall not look at build machine processor capabilities ([CPU](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/compile-time-check/cpu-detection))
   1. Thou shall not look at build machine [benchmarks for optimizations](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/compile-time-check/benchmark)
   1. Thou shall be careful with [profile-guided-optimization](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/pgo) for it can amplify any sin (non-determinism)
   1. Thou shall keep your workspace [environment](https://github.com/bmwiedemann/theunreproduciblepackage/tree/master/environment) clean of timezones, locales and umasks or ensure they do no harm
   1. Thou shall allow for offline builds (aka "vendoring" as servers can be down, contents can change)
   1. If Thou publishst binaries, Thou shall take note of your build inputs

License: [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
